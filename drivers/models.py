from django.db import models
from custom_users.models import User
# Create your models here.

class DriverExtraInfo(models.Model):
    CustomuserID = models.OneToOneField(User, on_delete = models.CASCADE,null=True,blank=True)
    image        = models.ImageField(null=True,blank=True)
    license_num  = models.CharField(max_length = 100,null=True,blank=True)
    created_at  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at  = models.DateTimeField(auto_now=True,)
    is_active    = models.BooleanField(default=True)
