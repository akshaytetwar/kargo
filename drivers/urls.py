from django.conf.urls import url

from . import views

urlpatterns = [
    url('api/driv_user', views.DriverCreate.as_view(), name='account-create'),
]
