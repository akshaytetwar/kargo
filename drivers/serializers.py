from rest_framework import serializers
from drivers.models import DriverExtraInfo
from custom_users.serializers import UserSerializer
class DriverExtraInfoSerializer(serializers.ModelSerializer):
    CustomuserID = UserSerializer(required=False)
    class Meta:
        model  = DriverExtraInfo
        exclude = ('created_at','updated_at','is_active')



      