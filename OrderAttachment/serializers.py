from rest_framework import serializers
from OrderAttachment.models import OrderAttachment

from orders.serializers import OrderSerializer
from custom_users.serializers import UserSerializer

class OrderAttachmentSerializer(serializers.ModelSerializer):
    OrderID = OrderSerializer(required=False)
    CustomUserID = UserSerializer(required=False)
    class Meta:
        model  = OrderAttachment
        exclude = ('created_at','updated_at','is_active')