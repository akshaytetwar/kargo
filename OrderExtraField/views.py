from django.shortcuts import render

from rest_framework import status
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework import status
from rest_framework.response import Response
from rest_framework import authentication, permissions
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken

from orders.models import Order
from status.models import Status
from custom_users.models import User


# Create your views here.
class CarrOrderDetails(APIView):
    def get(self, request ):
        print(request.GET["pk"])
        print(request.GET["id"])
        try:
            obj_order =  Order.objects.filter(CarrierCompanyID = int(request.GET["id"]), id = int(request.GET["pk"]))
        
            job_no = obj_order.values('id')
            job_no = (job_no[0])['id']
            
            order_placed = ((obj_order.values("created_at"))[0])["id"]
            
            crea_pk  = ((obj_order.values("CustomUserID"))[0])["CustomUserID"]
            crea_obj = User.objects.get(id = crea_pk)
            create_name = crea_obj.user_name

            cust_pk = ((obj_order.values("CustomerCompanyID"))[0])["CustomerCompanyID"]
            cust_obj = User.objects.get(id = cust_pk)
            cust_name = cust_obj.user_name

            stat_pk = ((obj_order.values("StatusID"))[0])["StatusID"]
            stat_obj = Status.objects.get(domainID = 2,id = stat_pk)
            stat_name = stat_obj.status_name 
            data = {
                "job_no":job_no,
                "cust_name":cust_name,
                "order_placed":order_placed,
                "create_name":create_name,
                "stat_name":stat_name
                    }
            context = {
              "data":data,
              "message":"api passed"
            }
            return Response(context,status=status.HTTP_200_OK)
        except Exception as e:
            context = {
                            "data": str(e),
                            "message":'Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
            