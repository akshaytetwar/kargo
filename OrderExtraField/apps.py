from django.apps import AppConfig


class OrderextrafieldConfig(AppConfig):
    name = 'OrderExtraField'
