from django.db import models
from OrderTemplate.models import OrderTemplate
from orders.models import Order

class OrderExtraField(models.Model):
    OrderID         = models.ForeignKey(Order, on_delete = models.CASCADE,null=True,blank=True)
    OrderTemplateID = models.ForeignKey(OrderTemplate, on_delete = models.CASCADE,null=True,blank=True)
    field_name      = models.CharField(max_length =200, null=True,blank=True)
    field_value     = models.CharField(max_length =200, null=True,blank=True)
    field_type      = models.CharField(max_length = 200, null=True,blank=True)
    created_at  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at  = models.DateTimeField(auto_now=True,)
    is_active       = models.BooleanField(default=True)
