from rest_framework import serializers
from OrderExtraField.models import OrderExtraField

from orders.serializers import OrderSerializer
from OrderTemplate.serializers import OrderTemplateSerializer

class OrderExtraFieldSerializer(serializers.ModelSerializer):
    OrderID = OrderSerializer(required=False)
    OrderTemplateID = OrderTemplateSerializer(required=False)
    class Meta:
        model  = OrderExtraField 
        exclude = ('created_at','updated_at','is_active')