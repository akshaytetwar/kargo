# Generated by Django 2.1 on 2019-02-23 12:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('OrderExtraField', '0004_auto_20190222_1735'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='orderextrafield',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='orderextrafield',
            name='updated_at',
        ),
        migrations.AddField(
            model_name='orderextrafield',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='orderextrafield',
            name='updated_at',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
