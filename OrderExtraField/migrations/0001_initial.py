# Generated by Django 2.1 on 2019-02-12 13:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('OrderTemplate', '0001_initial'),
        ('orders', '0002_auto_20190212_1840'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrderExtraField',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('field_name', models.CharField(max_length=200)),
                ('field_value', models.CharField(max_length=200)),
                ('field_type', models.CharField(max_length=200)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
                ('OrderID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='orders.Order')),
                ('OrderTemplateID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='OrderTemplate.OrderTemplate')),
            ],
        ),
    ]
