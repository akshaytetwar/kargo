from django.db import models
from custom_users.models import User
from company.models import Company
from status.models import Status
from domain.models import Domain
from user_role.models import UserRole

from django.core.validators import RegexValidator

# Create your models here.
class Order(models.Model):
    alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', 'Only alphanumeric characters are allowed.')

    domainID             = models.ForeignKey(Domain, on_delete = models.CASCADE, null=True,blank=True)
    roleID               = models.ForeignKey(UserRole, on_delete = models.CASCADE, null=True,blank=True)
    CustomUserID         = models.ForeignKey(User, on_delete = models.CASCADE,null=True,blank=True)
    CustomerCompanyID    = models.ForeignKey(Company,related_name='customer_order',on_delete = models.CASCADE,null=True,blank=True)
    CarrierCompanyID     = models.ForeignKey(Company, on_delete = models.CASCADE,null=True,blank=True)
    StatusID             = models.ForeignKey(Status, on_delete = models.CASCADE,null=True,blank=True)
    job_id               = models.CharField(max_length=50, blank=False, null=True, validators=[alphanumeric])
    expected_pickup_DT   = models.DateTimeField(null=True,blank=True)
    expected_delivery_DT = models.DateTimeField(null=True,blank=True)
    pickup_location      = models.CharField(max_length = 500,null=True,blank=True)
    delivery_location    = models.CharField(max_length = 500,null=True,blank=True)
    actual_pickup_TD     = models.DateTimeField(null=True,blank=True)
    actual_delivery_TD   = models.DateTimeField(null=True,blank=True)
    created_at  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at  = models.DateTimeField(auto_now=True,)
    is_active            = models.BooleanField(default=True)