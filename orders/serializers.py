from rest_framework import serializers
from orders.models import Order

from domain.serializers import DomainSerializer
from user_role.serializers import UserRoleSerializer
from custom_users.serializers import UserSerializer
from company.serializer import CompanySerializer
from status.serializers import StatusSerializer

class OrderSerializer(serializers.ModelSerializer):
    domainID = DomainSerializer(required=False)
    roleID= UserRoleSerializer(required=False)
    CustomUserID = UserSerializer(required=False)
    CustomerCompanyID = CompanySerializer(required=False)
    CarrierCompanyID   = CompanySerializer(required=False)
    StatusID  = StatusSerializer(required=False)
    class Meta:
        model  = Order
        exclude = ('created_at','updated_at','is_active')