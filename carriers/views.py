from django.shortcuts import render

from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework import status


from carriers.models import CarrierExtraInfo
from orders.models import Order
from status.models import Status
from custom_users.models import User
from company.models import Company
from drivers.models import DriverExtraInfo

from CarrierDriver.serializers import CarrierDriverSerializer


from TruckType.models import TruckType
from OrderDriver.models import OrderDriver
from CarrierDriver.models import CarrierDriver
from CustomerCarrier.models import CustomerCarrier
from CompanyUser.models import CompanyUser
from domain.models import Domain
from user_role.models import UserRole
from django.db.models import Q
#from carriers.serializers import CarrierCreateSerializer



from django.contrib.auth import authenticate

from custom_users.models import User

class Carrier(APIView):
    def post(self, request, format='json'):      #create
        try:
            data = request.data
            print(data['domainID'])
            domain = Domain.objects.get(id = data['domainID'])
            print(domain)
            role   = UserRole.objects.get(id = data['roleID'])
            print(role)
            
            user_obj = User(
                                                            email       = data['email'],
                                                            domainID    = domain,
                                                            roleID      = role,
                                                            user_name   = data['user_name'],
                                                            phone       = data['phone']
                                                    )  
            
            user_obj.set_password(data['password'])                                              
            user_obj.save()
            
            carr_obj = CarrierExtraInfo.objects.create(CustomUserID=user_obj,somevalue=data['CustomUserID']['somevalue'])        
            context = {
                "data": data['user_name'],
                "message": 'verify your account'
            }
            carr_obj.save()
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            context = {
                "data": str(e),
                "message": 'invalid details'
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)

'''    def put(self, request):                      #Update
        try:
            mod_obj = CarrierExtraInfo()
            serializer = CarrierCreateSerializer(mod_obj, data = request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
'''

class LogIn(APIView):                       #login
    def post(self, request, format='json'):
        try:
            user = authenticate(email = request.data['email'], password = request.data['password'])
            print(type(user))
            if user is not None:
                user_obj = Token.objects.filter(user=user).first()
                if user_obj:
                    user_obj.delete()                                    
                token = Token.objects.create(user=user)
                ob = CompanyUser.objects.filter(UserID = user.id)
                carr_com_id = ((ob.values('CompanyID'))[0])['CompanyID']
                li =[]
                obj_orders =  Order.objects.filter(CarrierCompanyID = carr_com_id)
                print("obj_orders------------------------------------",obj_orders)
                for obj_orders in obj_orders:
                    
                    cust_pk = obj_orders.CustomerCompanyID.id
                    cust_obj = Company.objects.get(id = cust_pk)
                    cust_name = cust_obj.name
                    cust_id   = cust_obj.id

                    stat_pk = obj_orders.StatusID.id
                    stat_obj = Status.objects.get(domainID = 2,id = stat_pk)
                    stat_name = stat_obj.status_name

                    job_id  = obj_orders.job_id

                    pickup_time   = obj_orders.expected_pickup_DT
                    
                    delivery_time = obj_orders.expected_delivery_DT
                    
                    
                    data = {
                    "cust_name":cust_name,
                    "cust_com_id":cust_id,
                    "job_id":job_id,
                    "pickup_time":pickup_time,
                    "delivery_time":delivery_time,
                    "status":stat_name,

                    }
                    li.append(data)

                context = {
                    "carr_id":carr_com_id,
                    "token": str(token.key),
                    "Order":li,
                    "message" : 'LoggedIn Successfully'
                }
                return Response(context,status=status.HTTP_200_OK)
            else:
                context = {
                    "data": 'None',
                    "message": 'Invalid Credentials'
                    }
                Response(context,status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            context = {
                            "data": str(e),
                            "message":'Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)


class StatusResult(APIView):                                             #status based result
    
    def get(self, request):
        print(request.GET['id'],request.GET['status'])
        try:
            if request.GET['status']:
                li = []
                obj = Order.objects.filter(CarrierCompanyID = request.GET['id'] , StatusID = request.GET['status'])
                for obj_orders in obj:

                    cust_pk = obj_orders.CustomerCompanyID.id
                    cust_obj = Company.objects.get(id = cust_pk)
                    cust_name = cust_obj.name

                    stat_pk = obj_orders.StatusID.id
                    stat_obj = Status.objects.get(domainID = 2,id = stat_pk)
                    stat_name = stat_obj.status_name

                    job_id  = obj_orders.job_id

                    pickup_time   = obj_orders.expected_pickup_DT
                    
                    delivery_time = obj_orders.expected_delivery_DT
                    
                    data = {
                    "cust_name":cust_name,
                    "job_id":job_id,
                    "pickup_time":pickup_time,
                    "delivery_time":delivery_time,
                    "status":stat_name,
                    }
                    li.append(data)


                context = {
                    "orders": li,
                    "message" : 'api passed'
                }
                return Response(context,status=status.HTTP_200_OK)
            else:
                context = {
                    "data": 'None',
                    "message": 'Error'
                    }
                Response(context,status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            context = {
                            "data": str(e),
                            "message":'Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)


class CarrDriverList(APIView):
    def get(self, request, format=None):
        # try:
        carr_dri_obj = CarrierDriver.objects.filter(CarrierCompanyID = request.GET['carr_id'])

        seri = CarrierDriverSerializer(carr_dri_obj, many =True)

        return Response(seri.data)
  

class CarrFilters(APIView):                                                     #carrier listing
    def get(self, request):
        try:
            if request.GET['filter']=='customer':
                obj_create = Order.objects.filter(CustomerCompanyID = request.GET["cust_id"], CarrierCompanyID = request.GET["carr_id"])
                li =[]
                for obj_orders in obj_create:
                    cust_pk   = obj_orders.CustomerCompanyID.id
                    cust_obj  = Company.objects.get(id = cust_pk)
                    cust_name = cust_obj.name
                    stat_pk   = obj_orders.StatusID.id
                    stat_obj  = Status.objects.get(domainID = 2,id = stat_pk)
                    stat_name = stat_obj.status_name
                    job_id    = obj_orders.job_id

                    pickup_time   = obj_orders.expected_pickup_DT
                        
                    delivery_time = obj_orders.expected_delivery_DT
                    data = {
                        "cust_name":cust_name,
                        "job_id":job_id,
                        "pickup_time":pickup_time,
                        "delivery_time":delivery_time,
                        "status":stat_name,
                        }
                    li.append(data)
                context = {
                        "data":li,
                        "report":"Successfully"
                    }
                return Response(context,status=status.HTTP_200_OK)
            

            elif request.GET["filter"]=='date':
                obj_create = Order.objects.filter(created_at = request.GET["cre_dt"], CarrierCompanyID = request.GET["carr_id"])
                li =[]
                for obj_orders in obj_create:
                    cust_pk   = obj_orders.CustomerCompanyID.id
                    cust_obj  = Company.objects.get(id = cust_pk)
                    cust_name = cust_obj.name
                    stat_pk   = obj_orders.StatusID.id
                    stat_obj  = Status.objects.get(domainID = 2,id = stat_pk)
                    stat_name = stat_obj.status_name
                    job_id    = obj_orders.job_id

                    pickup_time   = obj_orders.expected_pickup_DT
                        
                    delivery_time = obj_orders.expected_delivery_DT
                    data = {
                    "cust_name":cust_name,
                        "job_id":job_id,
                        "pickup_time":pickup_time,
                        "delivery_time":delivery_time,
                        "status":stat_name,
                        }
                    li.append(data)
                context = {
                        "data":li,
                        "report":"Successfully"
                    }
                return Response(context,status=status.HTTP_200_OK)

            elif request.GET["filter"]=='pickup':
                obj_create = Order.objects.filter(expected_pickup_DT = request.GET["pic_dt"], CarrierCompanyID = request.GET["carr_id"])
                li =[]
                for obj_orders in obj_create:
                    cust_pk   = obj_orders.CustomerCompanyID.id
                    cust_obj  = Company.objects.get(id = cust_pk)
                    cust_name = cust_obj.name
                    stat_pk   = obj_orders.StatusID.id
                    stat_obj  = Status.objects.get(domainID = 2,id = stat_pk)
                    stat_name = stat_obj.status_name
                    job_id    = obj_orders.job_id

                    pickup_time   = obj_orders.expected_pickup_DT
                        
                    delivery_time = obj_orders.expected_delivery_DT
                    data = {
                    "cust_name":cust_name,
                        "job_id":job_id,
                        "pickup_time":pickup_time,
                        "delivery_time":delivery_time,
                        "status":stat_name,
                        }
                    li.append(data)
                context = {
                        "data":li,
                        "report":"Successfully"
                    }
                return Response(context,status=status.HTTP_200_OK)

            elif request.GET["filter"]=='delivery':
                obj_create = Order.objects.filter(expected_delivery_DT = request.GET["del_dt"], CarrierCompanyID = request.GET["carr_id"])
                li =[]
                for obj_orders in obj_create:
                    cust_pk   = obj_orders.CustomerCompanyID.id
                    cust_obj  = Company.objects.get(id = cust_pk)
                    cust_name = cust_obj.name
                    stat_pk   = obj_orders.StatusID.id
                    stat_obj  = Status.objects.get(domainID = 2,id = stat_pk)
                    stat_name = stat_obj.status_name
                    job_id    = obj_orders.job_id

                    pickup_time   = obj_orders.expected_pickup_DT
                        
                    delivery_time = obj_orders.expected_delivery_DT
                    data = {
                    "cust_name":cust_name,
                        "job_id":job_id,
                        "pickup_time":pickup_time,
                        "delivery_time":delivery_time,
                        "status":stat_name,
                        }
                    li.append(data)
                context = {
                        "data":li,
                        "report":"Successfully"
                    }
                return Response(context,status=status.HTTP_200_OK)

            elif request.GET["filter"]=='status':
                obj_create = Order.objects.filter(StatusID = request.GET["sta_id"], CarrierCompanyID = request.GET["carr_id"])
                li =[]
                for obj_orders in obj_create:
                    cust_pk   = obj_orders.CustomerCompanyID.id
                    cust_obj  = Company.objects.get(id = cust_pk)
                    cust_name = cust_obj.name
                    stat_pk   = obj_orders.StatusID.id
                    stat_obj  = Status.objects.get(domainID = 2,id = stat_pk)
                    stat_name = stat_obj.status_name
                    job_id    = obj_orders.job_id

                    pickup_time   = obj_orders.expected_pickup_DT
                        
                    delivery_time = obj_orders.expected_delivery_DT
                    data = {
                    "cust_name":cust_name,
                        "job_id":job_id,
                        "pickup_time":pickup_time,
                        "delivery_time":delivery_time,
                        "status":stat_name,
                        }
                    li.append(data)
                context = {
                        "data":li,
                        "report":"Successfully"
                    }
                return Response(context,status=status.HTTP_200_OK)     


        except Exception as e:
            context = {
                                "data": str(e),
                                "message":'Error'
                        }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)




def search_name(q_,carr_id):
    obj_cus_carr = CustomerCarrier.objects.filter(CarriercompanyID = carr_id).values_list("CustomercompanyID_id")
    company_id_list = Company.objects.filter(id__in = obj_cus_carr, name__contains = q_).values_list("id")
    
    Order_list  = Order.objects.filter(CustomerCompanyID__in = company_id_list, CarrierCompanyID = carr_id)
    print(Order_list)
    return Order_list

def search_status(q_,carr_id):
    st_obj = Status.objects.filter(status_name__contains = q_).values_list("id")
    if st_obj:
        
        order_obj = Order.objects.filter(CarrierCompanyID = carr_id ,StatusID__in = st_obj)
        return order_obj
    else:
        return st_obj
def search_dt(q_,carr_id):

    order_obj = Order.objects.filter(CarrierCompanyID = carr_id ,created_DT__contains=q_)
    return order_obj         

def search_pick(q_,carr_id):
    order_obj = Order.objects.filter(CarrierCompanyID = carr_id ,expected_pickup_DT__contains=q_)
    return order_obj

def search_deli(q_,carr_id):
    order_obj = Order.objects.filter(CarrierCompanyID = carr_id ,expected_delivery_DT__contains=q_)
    return order_obj

def search_job(q_,carr_id):

    order_obj = Order.objects.filter(CarrierCompanyID = carr_id ,job_id__contains=q_)
    print("job----------------",order_obj)
    return order_obj

class CarrSearch(APIView):                                              #carrier search on dashbord
    def get(self, request):
        carr_id = request.GET["carr_id"]
        q_search = request.GET["search"]
        print('typeeeeeeeeeeeeeeeeeeeeeeeeee',type(q_search))
        q =[]
        q.append(search_status(q_search,carr_id))
        
        q.append(search_dt(q_search,carr_id))
        q.append(search_deli(q_search,carr_id))
        q.append(search_job(q_search,carr_id))
        q.append(search_pick(q_search,carr_id))
        q.append(search_name(q_search,carr_id))
        print()

        r_status = search_status(q_search,carr_id)
        r_dt = search_dt(q_search,carr_id)
        r_job = search_job(q_search,carr_id)
        r_pick = search_pick(q_search,carr_id)
        r_name = search_name(q_search,carr_id)
        r_deli = search_deli(q_search,carr_id)
        print("print()---------",r_deli )
        
        Final_order = r_status.union(r_deli,r_job,r_pick,r_name)
        print("============", Final_order.count())



        li = []
        ex = []
        for qs in q:
            for obj_orders in qs:              
                if obj_orders.id not in ex:
                    ex.append(obj_orders.job_id)            
                    cust_pk = obj_orders.CustomerCompanyID.id
                    cust_obj = Company.objects.get(id = cust_pk)
                    cust_name = cust_obj.name
                    cust_id   = cust_obj.id

                    stat_pk = obj_orders.StatusID.id
                    stat_obj = Status.objects.get(domainID = 2,id = stat_pk)
                    stat_name = stat_obj.status_name

                    job_id  = obj_orders.job_id

                    pickup_time   = obj_orders.expected_pickup_DT
                    
                    delivery_time = obj_orders.expected_delivery_DT
                    
                    
                    data = {
                    "cust_name":cust_name,
                    "cust_com_id":cust_id,
                    "job_id":job_id,
                    "pickup_time":pickup_time,
                    "delivery_time":delivery_time,
                    "status":stat_name,

                    }
                    li.append(data)
        print(ex)
        context = {
            "Order":li,
            "message" : 'success'
        }
        return Response(context,status=status.HTTP_200_OK)