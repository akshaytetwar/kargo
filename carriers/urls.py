from django.conf.urls import url
from carriers.views import *


urlpatterns = [
    url('api/carr_user', Carrier.as_view(), name='account-create'),
    url('api/login', LogIn.as_view(), name='login'),
    url('api/status/', StatusResult.as_view(), name = 'status-result'),
    url('api/drivlist/', CarrDriverList.as_view(), name = 'carr-driver'),
    url('api/filter/', CarrFilters.as_view(), name = 'carr-filter' ),
    url('api/search/', CarrSearch.as_view(), name ='carr-search')
]