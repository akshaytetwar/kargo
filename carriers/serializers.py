from rest_framework import serializers
from carriers.models import CarrierExtraInfo

from custom_users.serializers import UserSerializer

class CarrierExtraInfoSerializer(serializers.ModelSerializer):
    CustomUserID = UserSerializer(required=False)
    class Meta:
        model = CarrierExtraInfo
        exclude = ('created_at','updated_at','is_active')


