from django.db import models

class Company(models.Model):
    CHOICES = (
    ('carrier', 'carrier'),
    ('customer', 'customer'),)
    company_type  = models.CharField(max_length = 100,choices=CHOICES,null=True,blank=True)
    is_individual = models.BooleanField(default=True)
    name          = models.CharField(max_length =100,null=True,blank=True)
    mobile_num    = models.CharField(max_length =100,null=True,blank=True)
    location      = models.CharField(max_length = 500,null=True,blank=True)

    is_active     = models.BooleanField(default=True)
    created_at  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at  = models.DateTimeField(auto_now=True,)