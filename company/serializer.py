from rest_framework import serializers
from company.models import Company
class CompanySerializer(serializers.ModelSerializer):
    
    class Meta:
        model  = Company
        exclude = ('created_at','updated_at','is_active')
