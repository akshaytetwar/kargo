from django.db import models
from user_role.models import UserRole
from domain.models import Domain
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)


class UserManager(BaseUserManager):
    def create_user(self, email, user_name, phone, password=None):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('Users must have an email address')
        if not password:
            raise ValueError('User must have password')
       
        if not user_name:
            raise ValueError('user must have user name')
        if not phone:
            raise ValueError('user must have phone')            

        user = self.model(
            email=self.normalize_email(email),
        )
        user.set_password(password)
        #user.domainID  = domain
        #user.roleID    = role
        user.user_name = user_name
        user.phone     = phone
        user.save(using=self._db)
        return user


    def create_staffuser(self, email, password, user_name, phone):
        """
        Creates and saves a staff user with the given email and password.
        """
        user = self.create_user(
            email,
            password  = password,
            #role     = roleID,
            user_name = user_name,
            phone     = phone
        )
        user.active      = False
        user.is_verified = False
        user.staff       = True
        user.save(using=self._db)
        return user
    

    def create_superuser(self, email, password, user_name, phone):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            email,
            password  = password,
            #role      = roleID,
            user_name = user_name,
            phone     = phone
        )
        user.is_verified = True
        user.staff       = True
        user.admin       = True
        
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    objects = UserManager()
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )

    domainID    =  models.ForeignKey(Domain, on_delete = models.CASCADE, null=True,blank=True)
    roleID      =  models.ForeignKey(UserRole, on_delete = models.CASCADE, null=True,blank=True)
    user_name   =  models.CharField(max_length = 200, null=True,blank=True) 
    phone       =  models.CharField(max_length = 50, null=True,blank=True)
    is_verified =  models.BooleanField(default=False)
    active      =  models.BooleanField(default=True)
    staff       =  models.BooleanField(default=False) # a admin user; non super-user
    admin       =  models.BooleanField(default=False) # a superuser
    created_at  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at  = models.DateTimeField(auto_now=True,)
    
    # notice the absence of a "Password field", that's built in.

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['user_name','phone'] # Email & Password are required by default.

    def get_full_name(self):
        # The user is identified by their email address
        return self.user_name

    def get_short_name(self):
        # The user is identified by their email address
        return self.user_name

    def __str__(self):              # __unicode__ on Python 2
        return '%s %s' % (self.user_name,self.roleID)

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        return self.staff

    @property
    def is_admin(self):
        "Is the user a admin member?"
        return self.admin

    @property
    def is_active(self):
        "Is the user active?"
        return self.active