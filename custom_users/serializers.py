from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.contrib.auth.models import User


from domain.serializers import DomainSerializer
from user_role.serializers import UserRoleSerializer


class UserSerializer(serializers.ModelSerializer):
    domainID = DomainSerializer(required=False)
    roleID  = UserRoleSerializer(required=False)
    
    class Meta:
        model = User
        fields = '__all__'
 