from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework import status

from django.contrib.auth import authenticate

from custom_users.models import User



# Create your views here.
class LogIn(APIView):
    def post(self, request, format='json'):
        try:
            user = authenticate(email = request.data['email'], password = request.data['password'])
            if user is not None:
                user_obj = Token.objects.filter(user=user).first()
                if user_obj:
                    user_obj.delete()                                    
                token = Token.objects.create(user=user)
                context = {
                    "data": str(token.key),
                    "message": 'LoggedIn Successfully'
                }
                return Response(context,status=status.HTTP_200_OK)
            else:
                context = {
                    "data": 'None',
                    "message": 'Invalid Credentials'
                    }
                return Response(context,status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            context = {
                            "data": str(e),
                            "message":'Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)