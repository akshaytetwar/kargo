from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
    path('api/cust_user', views.CustomerCreate.as_view(), name='account-create'),
]
