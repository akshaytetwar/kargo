from django.shortcuts import render

from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions

from custom_users.models import User
from .models import CustomerExtraInfo
# Create your views here.

class CustomerCreate(APIView):
    def post(self, request, format='json'):
        try:
            data = request.data
            user_obj = User(
                                                            email       = data['email'],
                                                            domainID_id = data['domainID'],
                                                            roleID_id   = data['roleID'],
                                                            user_name   = data['user_name'],
                                                            phone       = data['phone']
                                                    )  
            
            user_obj.set_password(data['password'])                                              
            user_obj.save()
            
            cust_obj = CustomerExtraInfo.objects.create(CustomUserID=user_obj,somevalue=data['CustomUserID']['somevalue'])        
            context = {
                "data": data['user_name'],
                "message": 'verify your account'
            }
            cust_obj.save()
            return Response(context,status=status.HTTP_200_OK)
        except Exception as e:
            context = {
                "data": str(e),
                "message": 'invalid details'
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)