from django.db import models
from custom_users.models import User

class CustomerExtraInfo(models.Model):
    CustomUserID   = models.OneToOneField(User, on_delete = models.CASCADE,null=True,blank=True)
    somevalue      = models.CharField(max_length=100,null=True,blank=True)
    is_active      = models.BooleanField(default=True)
    
    created_at  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at  = models.DateTimeField(auto_now=True,)
