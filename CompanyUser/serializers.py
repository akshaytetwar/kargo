from rest_framework import serializers
from CompanyUser.models import CompanyUser

from company.serializer import CompanySerializer
from custom_users.serializers import UserSerializer
from CompanyUserRole.serializers import CompanyUserRoleSerializer

class CompanyUserSerializer(serializers.ModelSerializer):
    CompanyID          = CompanySerializer(required=False)
    UserID             = UserSerializer(required=False)
    CompanyUserRoleID  = CompanyUserRoleSerializer(required=False)
    class Meta:
        model  = CompanyUser
        exclude = ('created_at','updated_at','is_active')