from django.db import models
from company.models import Company
from custom_users.models import User
from CompanyUserRole.models import CompanyUserRole

class CompanyUser(models.Model):
    CompanyID            = models.ForeignKey(Company,on_delete = models.CASCADE,null=True,blank=True)
    UserID               = models.ForeignKey(User,on_delete = models.CASCADE,null=True,blank=True)
    CompanyUserRoleID    = models.ForeignKey(CompanyUserRole, on_delete = models.CASCADE, null=True,blank=True)
    is_active            = models.BooleanField(default=True)
    
    created_at  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at  = models.DateTimeField(auto_now=True,)