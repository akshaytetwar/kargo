from rest_framework import serializers
from DomainRelatedOrderExtraField.models import DomainRelatedOrderExtraField

from domain.serializers import DomainSerializer

class DomainRelatedOrderExtraFieldSerializer(serializers.ModelSerializer):
    DomainID = DomainSerializer(required=False)
    class Meta:
        model  = DomainRelatedOrderExtraField 
        exclude = ('created_at','updated_at','is_active')
