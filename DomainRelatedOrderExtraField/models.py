from django.db import models
from domain.models import Domain

class DomainRelatedOrderExtraField(models.Model):
    DomainID    = models.ForeignKey(Domain, on_delete = models.CASCADE,null=True,blank=True)
    field_name  = models.CharField(max_length = 200,null=True,blank=True)
    field_type  = models.CharField(max_length =200,null=True,blank=True)
    group_label = models.CharField(max_length = 100,null=True,blank=True)  
    created_at  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at  = models.DateTimeField(auto_now=True,)
    is_active   = models.BooleanField(default=True)
