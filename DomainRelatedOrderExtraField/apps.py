from django.apps import AppConfig


class DomainrelatedorderextrafieldConfig(AppConfig):
    name = 'DomainRelatedOrderExtraField'
