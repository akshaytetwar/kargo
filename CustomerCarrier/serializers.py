from rest_framework import serializers
from CustomerCarrier.models import CustomerCarrier

from company.serializer import CompanySerializer
class CustomerCarrierSerializer(serializers.ModelSerializer):
    CustomercompanyID = CompanySerializer(required=False)
    CarriercompanyID  = CompanySerializer(required=False)
    class Meta:
        model  = CustomerCarrier
        exclude = ('created_at','updated_at','is_active')
        