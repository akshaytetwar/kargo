from django.db import models
from company.models import Company

class CustomerCarrier(models.Model):
    CustomercompanyID   = models.ForeignKey(Company,related_name='customer_company',on_delete = models.CASCADE,null=True,blank=True)
    CarriercompanyID    = models.ForeignKey(Company, on_delete = models.CASCADE,null=True,blank=True)
    rating              = models.FloatField(null=True,blank=True)
    created_at  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at  = models.DateTimeField(auto_now=True,)
    is_active           = models.BooleanField(default=True)
