from rest_framework import serializers

class Serializer(serializers.ModelSerializer):
    
    class Meta:
        model  = 
        exclude = ('created_at','updated_at','is_active')