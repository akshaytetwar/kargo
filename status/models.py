from django.db import models
from domain.models import Domain

# Create your models here.
class Status(models.Model):
    domainID    = models.ForeignKey(Domain, on_delete = models.CASCADE, null=True,blank=True)
    status_name = models.CharField(max_length = 50, null=True,blank=True)
    created_at  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at  = models.DateTimeField(auto_now=True,)
    is_active   = models.BooleanField(default=True)
