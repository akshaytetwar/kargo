# Generated by Django 2.1 on 2019-02-12 10:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('domain', '0001_initial'),
        ('user_role', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status_name', models.CharField(max_length=50)),
                ('is_active', models.BooleanField(default=True)),
                ('DomainID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='domain.Domain')),
                ('RoleID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user_role.UserRole')),
            ],
        ),
    ]
