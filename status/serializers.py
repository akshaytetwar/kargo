from rest_framework import serializers
from status.models import Status

from domain.serializers import DomainSerializer

class StatusSerializer(serializers.ModelSerializer):
    domainID = DomainSerializer(required=False)
    class Meta:
        model  = Status
        exclude = ('is_active',)
        