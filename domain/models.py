from django.db import models

# Create your models here.
class Domain(models.Model):
    name        = models.CharField(max_length = 100, null=True,blank=True)
    description = models.CharField(max_length = 500, null=True,blank=True)
    created_at  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at  = models.DateTimeField(auto_now=True,)
    is_active   = models.BooleanField(default=True)
