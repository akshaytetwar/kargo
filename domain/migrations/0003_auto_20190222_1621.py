# Generated by Django 2.1 on 2019-02-22 10:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('domain', '0002_auto_20190222_1551'),
    ]

    operations = [
        migrations.AlterField(
            model_name='domain',
            name='description',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='domain',
            name='name',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
