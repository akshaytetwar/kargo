from django.apps import AppConfig


class OrderdriverConfig(AppConfig):
    name = 'OrderDriver'
