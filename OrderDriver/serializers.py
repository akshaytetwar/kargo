from rest_framework import serializers
from OrderDriver.models import OrderDriver

from orders.serializers import OrderSerializer
from custom_users.serializers import UserSerializer

class OrderDriverSerializer(serializers.ModelSerializer):
    OrderID = OrderSerializer(required=False)
    DriverUserID = UserSerializer(required=False)
    class Meta:
        model  = OrderDriver
        exclude = ('created_at','updated_at','is_active')