from django.db import models
from orders.models import Order
from custom_users.models import User

class OrderComment(models.Model):
    OrderID      = models.ForeignKey(Order, on_delete = models.CASCADE,null=True,blank=True)
    CustomUserID = models.ForeignKey(User, on_delete = models.CASCADE,null=True,blank=True)
    comment      = models.CharField(max_length = 500,null=True,blank=True)
    created_at  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at  = models.DateTimeField(auto_now=True,)
    is_active    = models.BooleanField(default=True)
    