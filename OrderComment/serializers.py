from rest_framework import serializers
from OrderComment.models import OrderComment

from orders.serializers import OrderSerializer
from custom_users.serializers import UserSerializer

class OrderCommentSerializer(serializers.ModelSerializer):
    OrderID = OrderSerializer(required=False)
    CustomUserID = UserSerializer(required=False)
    class Meta:
        model  = OrderComment
        exclude = ('created_at','updated_at','is_active')