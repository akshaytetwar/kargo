# Generated by Django 2.1 on 2019-02-22 12:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('OrderComment', '0002_auto_20190222_1722'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ordercomment',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
    ]
