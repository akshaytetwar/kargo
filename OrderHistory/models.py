from django.db import models

from orders.models import Order
from status.models import Status
from custom_users.models import User
# Create your models here.

class OrderHistory(models.Model):
    OrderID = models.ForeignKey(Order, on_delete = models.CASCADE,null=True,blank=True)
    StatusID = models.ForeignKey(Status, on_delete = models.CASCADE,null=True,blank=True)
    created_by = models.ForeignKey(User, on_delete = models.CASCADE,null=True,blank=True)
    comments = models.CharField(max_length=50,null=True, blank=False)
    created_at  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at  = models.DateTimeField(auto_now=True,)