from rest_framework import serializers
from OrderHistory.models import OrderHistory

from orders.serializers import OrderSerializer
from status.serializers import StatusSerializer
from custom_users.serializers import UserSerializer

class OrderHistorySerializer(serializers.ModelSerializer):
    OrderID = OrderSerializer(required=False)
    StatusID = StatusSerializer(required=False)
    created_by = UserSerializer(required=False)
    class Meta:
        model  = OrderHistory
        exclude = ('created_at','updated_at','is_active')
