from rest_framework import serializers
from CarrierDriver.models import CarrierDriver

from company.serializer import CompanySerializer
from custom_users.serializers import UserSerializer
from status.serializers import StatusSerializer

class CarrierDriverSerializer(serializers.ModelSerializer):
    CarrierCompanyID = CompanySerializer(required=False)
    DriverUserID     = UserSerializer(required=False)
    status           = StatusSerializer(required=False)

    class Meta:
        model = CarrierDriver
        exclude = ('created_at','updated_at','is_active')




