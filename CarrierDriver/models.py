from django.db import models
from company.models import Company
from custom_users.models import User
from status.models import Status

class CarrierDriver(models.Model):
    CarrierCompanyID   = models.ForeignKey(Company, on_delete = models.CASCADE,null=True,blank=True)
    DriverUserID       = models.ForeignKey(User, on_delete = models.CASCADE,null=True,blank=True)
    status             = models.ForeignKey(Status, on_delete = models.CASCADE, null=True,blank=True)
    rating             = models.FloatField(null=True,blank=True)
    is_active          = models.BooleanField(default=True)
    created_at         = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at         = models.DateTimeField(auto_now=True,)
