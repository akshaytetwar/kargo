from django.apps import AppConfig


class CarrierdriverConfig(AppConfig):
    name = 'CarrierDriver'
