from rest_framework import serializers
from OrderTemplate.models import OrderTemplate

from custom_users.serializers import UserSerializer

class OrderTemplateSerializer(serializers.ModelSerializer):
    UserID = UserSerializer(required=False)
    class Meta:
        model  = OrderTemplate
        exclude = ('created_at','updated_at','is_active')