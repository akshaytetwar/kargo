from django.apps import AppConfig


class OrdertemplateConfig(AppConfig):
    name = 'OrderTemplate'
