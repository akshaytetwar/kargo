from django.db import models
from custom_users.models import User

class OrderTemplate(models.Model):
    UserID        = models.ForeignKey(User, on_delete = models.CASCADE, null=True,blank=True)
    template_name = models.CharField(max_length =200, null=True,blank=True)
    created_at  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at  = models.DateTimeField(auto_now=True,)
    is_active     = models.BooleanField(default=True)

