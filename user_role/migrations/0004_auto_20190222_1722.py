# Generated by Django 2.1 on 2019-02-22 11:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_role', '0003_auto_20190222_1550'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userrole',
            name='DomainID',
            field=models.ManyToManyField(blank=True, null=True, to='domain.Domain'),
        ),
        migrations.AlterField(
            model_name='userrole',
            name='role_name',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
