from rest_framework import serializers
from user_role.models import UserRole

from domain.serializers import DomainSerializer

class UserRoleSerializer(serializers.ModelSerializer):
    DomainID = DomainSerializer(required=False)
    class Meta:
        model  = UserRole
        exclude = ('created_at','updated_at','is_active')