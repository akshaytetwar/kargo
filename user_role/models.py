from django.db import models
from domain.models import Domain
# Create your models here.

class UserRole(models.Model):
    DomainID  = models.ForeignKey(Domain, null=True,blank=True, on_delete = models.CASCADE)
    role_name = models.CharField(max_length = 50,null=True,blank=True)
    created_at  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at  = models.DateTimeField(auto_now=True,)
    is_active   = models.BooleanField(default=True)

