from rest_framework import serializers
from TruckType.models import TruckType

from drivers.serializers import DriverExtraInfoSerializer

class TruckTypeSerializer(serializers.ModelSerializer):
    driver_extra_info = DriverExtraInfoSerializer(required=False)
    class Meta:
        model  = TruckType 
        exclude = ('created_at','updated_at','is_active')