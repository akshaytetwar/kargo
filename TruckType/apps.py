from django.apps import AppConfig


class TrucktypeConfig(AppConfig):
    name = 'TruckType'
