from django.db import models
from drivers.models import DriverExtraInfo

class TruckType(models.Model):
    driver_extra_info = models.ForeignKey(DriverExtraInfo, on_delete = models.CASCADE, null=True,blank=True)
    truck_reg_num     = models.CharField(max_length = 100, null=True,blank=True)
    created_at  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at  = models.DateTimeField(auto_now=True,)
    is_active         = models.BooleanField(default=True)
