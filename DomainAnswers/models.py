from django.db import models
from DomainRelatedOrderExtraField.models import DomainRelatedOrderExtraField
from orders.models import Order
from custom_users.models import User

class DomainAnswers(models.Model):
    OrderID                        = models.ForeignKey(Order, on_delete = models.CASCADE,null=True,blank=True)
    CustomUserID                   = models.ForeignKey(User, on_delete = models.CASCADE,null=True,blank=True)
    DomainRelatedOrderExtraFieldID = models.ForeignKey(DomainRelatedOrderExtraField, on_delete = models.CASCADE,null=True,blank=True)
    field_value                    = models.CharField(max_length = 200, null=True,blank=True)   
    created_at  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at  = models.DateTimeField(auto_now=True,)
    is_active                      = models.BooleanField(default=True)