from rest_framework import serializers
from DomainAnswers.models import DomainAnswers

from orders.serializers import OrderSerializer
from custom_users.serializers import UserSerializer

class DomainAnswersSerializer(serializers.ModelSerializer):
    OrderID = OrderSerializer(required=False)
    CustomUserID = UserSerializer(required=False)
    DomainRelatedOrderExtraFieldID = DomainRelatedOrderExtraFieldSerializer(required=False)
    class Meta:
        model  = DomainAnswers
        exclude = ('created_at','updated_at','is_active')