# Generated by Django 2.1 on 2019-02-22 11:52

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('DomainAnswers', '0003_auto_20190216_1111'),
    ]

    operations = [
        migrations.AlterField(
            model_name='domainanswers',
            name='CustomUserID',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='domainanswers',
            name='DomainRelatedOrderExtraFieldID',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='DomainRelatedOrderExtraField.DomainRelatedOrderExtraField'),
        ),
        migrations.AlterField(
            model_name='domainanswers',
            name='OrderID',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='orders.Order'),
        ),
        migrations.AlterField(
            model_name='domainanswers',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AlterField(
            model_name='domainanswers',
            name='is_active',
            field=models.BooleanField(blank=True, default=True, null=True),
        ),
        migrations.AlterField(
            model_name='domainanswers',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
    ]
